package ru.t1.skasabov.tm.api.service;

import ru.t1.skasabov.tm.model.Project;

import java.util.List;

public interface IProjectService {

    Project add(Project project);

    Project create(String name, String description);

    Project create(String name);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

    List<Project> findAll();

    void clear();

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    void remove(Project project);

    Project removeById(String id);

    Project removeByIndex(Integer index);

}
